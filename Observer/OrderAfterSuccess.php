<?php
namespace Littlelunch\AddressValidator\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Event\Observer;
use Magento\Checkout\Model\Session;
use Magento\Framework\App\ResponseFactory;
use Magento\Framework\UrlInterface;
use Magento\Framework\App\Request\Http;
use Littlelunch\AddressValidator\Helper\Validator;

class OrderAfterSuccess implements ObserverInterface
{

    protected $PATH_TO_FORM = 'littlelunch/address/address';

    protected $_checkoutSession;

    protected $_responseFactory;

    protected $_url;

    protected $_requets;

    protected $_validator;

    public function __construct(
        ResponseFactory $responseFactory,
        UrlInterface $url,
        Session $checkoutSession,
        Validator $helper,
        Http $request
    ) {
        $this->_checkoutSession = $checkoutSession;
        $this->_url = $url;
        $this->_responseFactory = $responseFactory;
        $this->_requets;
        $this->_validator = $helper;
    }

    /**
     *
     * Add data to section array for custumer data use
     *
     */

    public function execute(Observer $observer) {  

        $input_address = $this->getInputAddress();

        $this->_validator->setInputAddress($input_address['street'], $input_address['city'], $input_address['country'], $input_address['postcode']);

        $this->_validator->loadAddressByGoogle();
        if ($this->_validator->compareAddress()) {
            return $this;
        }
        $this->redirectToForm();
        
        exit;
    }

    protected function redirectToForm() {

        $query = ['google_data'=>
                    json_encode($this->_validator->getAddressByGoogle()),
                  'user_data'=>
                    json_encode($this->_validator->getInputAdress())];

        $customerBeforeAuthUrl = $this->_url
                        ->getUrl($this->PATH_TO_FORM,[
                                '_query'=>$query]);
        var_dump($customerBeforeAuthUrl); exit;

        $this->responseFactory->create()
                          ->setRedirect($customerBeforeAuthUrl)
                          ->sendResponse();
    }

    protected function getInputAddress() {
        $order = $this->_checkoutSession->getLastRealOrder();
        $billingAddress = $order->getBillingAddress();

        $input_address = array();


        $street = $billingAddress->getStreet();
        $input_address['street'] = $street[0];

        $input_address['city'] = $billingAddress->getCity();

        $input_address['postcode'] = $billingAddress->getPostcode();

        $country_id = $billingAddress->getCountryId();

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();

        $input_address['country']  = $objectManager
                            ->create('\Magento\Directory\Model\Country')
                            ->load($country_id)
                            ->getName();

        return $input_address;
    }

}